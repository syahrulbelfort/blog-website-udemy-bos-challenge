const homeStartingContent =
  'Lacus vel facilisis volutpat est velit egestas dui id ornare. Semper auctor neque vitae tempus quam. Sit amet cursus sit amet dictum sit amet justo. Viverra tellus in hac habitasse. Imperdiet proin fermentum leo vel orci porta. Donec ultrices tincidunt arcu non sodales neque sodales ut. Mattis molestie a iaculis at erat pellentesque adipiscing. Magnis dis parturient montes nascetur ridiculus mus mauris vitae ultricies. Adipiscing elit ut aliquam purus sit amet luctus venenatis lectus. Ultrices vitae auctor eu augue ut lectus arcu bibendum at. Odio euismod lacinia at quis risus sed vulputate odio ut. Cursus mattis molestie a iaculis at erat pellentesque adipiscing.';
const _ = require('lodash');

let posts = [];

module.exports = {
  index: (req, res) => {
    res.render('home', { startingContent: homeStartingContent, posts });
  },
  compose: (req, res) => {
    let post = {
      title: req.body.postTitle,
      content: req.body.postBody,
    };
    posts.push(post);
    res.redirect('/');
  },
  postName: (req, res) => {
    const reqTitle = _.lowerCase(req.params.postName);
    posts.forEach(function (post) {
      const storedTitle = _.lowerCase(post.title);
      if (storedTitle === reqTitle) {
        res.render('post', { title: post.title, content: post.content });
      }
    });
  },
};
