const express = require('express');
const router = express.Router();

const homeRoute = require('../controllers/homeController');
router.get('/', homeRoute.index);

const contactRoute = require('../controllers/contactController');
router.get('/contact', contactRoute.contact);

const aboutRoute = require('../controllers/aboutController');
router.get('/about', aboutRoute.about);

router.get('/compose', (req, res) => {
  res.render('compose');
});

router.post('/compose', homeRoute.compose);

router.get('/posts/:postName', homeRoute.postName);

module.exports = router;
